﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformPusher : MonoBehaviour {
    Rigidbody seinBody;
    ControllerColliderHit hitter;
    private float pushPower = 2.0f;
	void Update () {
        Collisioning(hitter);
	}
    public void Collisioning(ControllerColliderHit hit) {
        seinBody = hit.collider.attachedRigidbody;
        if (seinBody == null || seinBody.isKinematic) {
            return;
        }
        if (hit.moveDirection.x < -0.3f) {
            return;
        }
        Vector3 pushDir = new Vector3(hit.moveDirection.x, 0, 0);
        seinBody.velocity = pushDir * pushPower;
    }
}
