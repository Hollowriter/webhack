﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HackingBehaviour : MonoBehaviour {
	private static int count = 0;
    private bool hackeado;
    private bool direction = false;
   //  private int indirizzo;
    [SerializeField]
    public float speed;
    [SerializeField]
    public int publicHackedTimer;
    private int hackedTimer;
    int timer;
	void Start(){
        hackeado = false;
		count = 1;
        // indirizzo = 1;
        if (this.tag == "hackeo") {
            speed = 23;
        }
        timer = 60;
        hackedTimer = publicHackedTimer;
	}
	void Update () {
        if (hackeado == false){
            transform.Translate(Vector3.left * speed * -1 * Time.deltaTime);
            if (timer > 0){
                timer--;
            }
            else{
                Destroy(this.gameObject);
            }
       }
       else if (hackeado == true && this.tag == "disparo") {
            hackedTimer--;
            if (hackedTimer <= 0){
                hackeado = false;
                hackedTimer = publicHackedTimer;
            }
        }
	}
    public int GetCount() {
        return count;
    }
    public void DondeVoy(bool determinado){
        direction = determinado;
    }
    void OnTriggerEnter(Collider other){
        if (hackeado == false){
            if (this.tag == "hackeo"){
                if (other.tag == "MovingPlatform" || other.tag == "cono" ||
					other.tag == "MovingWall" || other.tag == "enemigo" || 
                    other.tag == "disparo" || other.tag == "pared"){
                    Destroy(this.gameObject);
                }
            }
            if (this.tag == "disparo"){
                if (other.tag == "MovingPlatform" || other.tag == "cono" ||
                    other.tag == "MovingWall" || other.tag == "Player" || other.tag == "pared"){
                    Destroy(this.gameObject);
                }
                else if (other.tag == "hackeo"){
                    hackeado = true;
                    Debug.Log("hackeado");
                }
            }
        }
    }
}
