﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterScript1 : MonoBehaviour {
    [SerializeField]
	private Rigidbody rbd;
    [SerializeField]
	private GameObject virus;
    [SerializeField]
    private GameObject spawnPoint;
    [SerializeField]
    private GameObject gameOver;
    Vector3 vel;
    RaycastHit hit;
    private int jumpCount;
    private int timeDead;
    private const int animationSpeedThingy = 3;
	private bool saltandoPresionado = false;
	private bool direccion = false;
    private bool muerta = false;
    private bool theWall = false;
    private bool pausa = false;
    private bool enElPiso = false;
    [SerializeField]
    private Animator anim;
    [SerializeField]
    private AudioClip disparar;
    [SerializeField]
    private AudioClip saltar;
    [SerializeField]
    private AudioClip caminar;
    [SerializeField]
    private AudioClip muerte;
    private int timeSonidoCaminar;

    void Start(){
        jumpCount = 0;
        timeDead = 0;
		rbd = GetComponent<Rigidbody> ();
        vel = Vector3.zero;
	}

	void Update () {
		Movement ();
        Hackeo();
        AnimationControllingThingy();
        Pausa();
        Muerte();
	}

    void Pausa(){
        if (Input.GetKeyDown(KeyCode.P)) {
            if (pausa == true) {
                Time.timeScale = 1;
                pausa = false;
            }
            else if (pausa == false){
                Time.timeScale = 0;
                pausa = true;
            }
        }
    }

	void Movement(){
        vel = rbd.velocity;
		if ((Input.GetKey (KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) && muerta == false) {
			vel.x += 50f * Time.deltaTime;
            if (vel.x > 10.0f){
                vel.x = 10.0f;
                if (timeSonidoCaminar == 0 && enElPiso == true){
                    GetComponent<AudioSource>().PlayOneShot(caminar);
                }
                if (timeSonidoCaminar < 15){
                    timeSonidoCaminar++;
                }
                else {
                    timeSonidoCaminar = 0;
                }
            }
            direccion = true;
		}
		if ((Input.GetKey (KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) && muerta == false && pausa == false) {
			vel.x += -50f * Time.deltaTime;
            if (vel.x < -10.0f){
                vel.x = -10.0f;
                if (timeSonidoCaminar == 0 && enElPiso == true && pausa == false){
                    GetComponent<AudioSource>().PlayOneShot(caminar);
                }
                if (timeSonidoCaminar < 15){
                    timeSonidoCaminar++;
                }
                else{
                    timeSonidoCaminar = 0;
                }
            }
			direccion = false;
		}
        if ((!(Input.GetKey(KeyCode.A)) && !(Input.GetKey(KeyCode.LeftArrow)) 
            && !(Input.GetKey(KeyCode.D)) && !(Input.GetKey(KeyCode.RightArrow))) 
            && vel.y == 0){
            vel.x = 0f;
        }
		if ((Input.GetKeyDown (KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) && jumpCount < 2 && muerta == false) {
                enElPiso = false;
                if (jumpCount < 1){
                    vel.y = 10f;
                    jumpCount++;
                    GetComponent<AudioSource>().PlayOneShot(saltar);
                }
                else {
                    vel.y = 15f;
                    jumpCount++;
                    GetComponent<AudioSource>().PlayOneShot(saltar);
                }
                if (theWall == true && direccion == true){
                    vel.x = -20f;
                    direccion = false;
                    transform.rotation = Quaternion.AngleAxis(180, Vector3.up);
                }
                else if (theWall == true && direccion == false){
                    vel.x = 20f;
                    direccion = true;
                    transform.rotation = Quaternion.AngleAxis(0, Vector3.up);
                }
		}
        // Posicion de origen del rayo
        // Direccion
        // Informacion de la interseccion del rayo con el collider
        // Tamaño del rayo
        /*if ((Physics.BoxCast(this.transform.position + Vector3.down * 0.7f, new Vector3(1.0f, 0.1f, 1.0f), Vector3.down, out hit, Quaternion.identity, 0.75f)) && vel.y < 0 && theWall == false){
            jumpCount = 0;
            enElPiso = true;
        }*/
        rbd.velocity = vel;
    }

    void AnimationControllingThingy(){
        if (muerta == false && theWall == false && enElPiso == true){
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)){
                anim.SetInteger("AnimIndex", 3);
                anim.speed = 1;
                transform.rotation = Quaternion.AngleAxis(0, Vector3.up);
            }
            else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)){
                anim.SetInteger("AnimIndex", 3);
                anim.speed = 1;
                transform.rotation = Quaternion.AngleAxis(180, Vector3.up);
            }
            else{
                anim.SetInteger("AnimIndex", 1);
            }
        }
        else if (muerta == false && theWall == false && enElPiso == false){
            anim.SetInteger("AnimIndex", 9);
            anim.speed = animationSpeedThingy;
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)){
                transform.rotation = Quaternion.AngleAxis(0, Vector3.up);
            }
            else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)){
                transform.rotation = Quaternion.AngleAxis(180, Vector3.up);
            }
        }
        else if (muerta == false && theWall == true){
            anim.speed = animationSpeedThingy;
            anim.SetInteger("AnimIndex", 452);
        }
        else if (muerta == true){
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("0058_Pose_Laydown")){
                timeDead++;
                transform.tag = "Muerta";
                if (timeDead >= 16){
                    if (Input.GetKey(KeyCode.Space)){
                        muerta = false;
                        transform.tag = "Player";
                        timeDead = 0;
                        Application.LoadLevel(Application.loadedLevel);
                    }
                    else if (Input.GetKey(KeyCode.Escape)){
                        SceneManager.LoadScene("Menu");
                    }
                }
            }
        }
    }

    private void FixedUpdate(){

    }

	void Hackeo(){
		if (Input.GetKeyDown (KeyCode.Space) && !(GameObject.Find("Hacking(Clone)") != null) && muerta == false) {
            GameObject leHackeo = (GameObject)Instantiate(virus, transform.position, transform.rotation);
            HackingBehaviour hacking = leHackeo.GetComponent<HackingBehaviour>();
            GetComponent<AudioSource>().PlayOneShot(disparar);
		}
	}

    void Muerte() {
        if (muerta == false){
            gameOver.SetActive(false);
        }
        else {
            gameOver.SetActive(true);
        }
    }

    private void OnTriggerEnter(Collider other){
		if (other.tag == "insDeath" && anim.GetInteger("AnimIndex") != 58 && muerta == false || 
            other.tag == "disparo" && anim.GetInteger("AnimIndex") != 58 && muerta == false) {
            muerta = true;
            anim.SetInteger("AnimIndex", 58);
            GetComponent<AudioSource>().PlayOneShot(muerte);
        }
        if (other.tag == "pared" || other.tag == "MovingWall"){
            theWall = true;
            jumpCount = 1;
        }
    }
    private void OnTriggerStay(Collider other){
        if (other.tag == "pared" || other.tag == "MovingWall"){
            theWall = true;
        }
        if (other.tag == "MovingPlatform" || other.tag == "piso" || other.tag == "enemigo"){
            jumpCount = 0;
            enElPiso = true;
        }
    }
    private void OnTriggerExit(Collider other){
        if ((other.tag == "pared" || other.tag == "MovingWall")
            && (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))){
            theWall = false;
            jumpCount = 2;
        }
        else if (other.tag == "pared" || other.tag == "MovingWall"){
            theWall = false;
            jumpCount = 1;
        }
        if ((other.tag == "MovingPlatform" || other.tag == "piso" || other.tag == "enemigo") 
            && (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))){
            enElPiso = false;
            jumpCount = 1;
        }
        else if (other.tag == "MovingPlatform" || other.tag == "piso" || other.tag == "enemigo"){
             enElPiso = false;
        }
    }
}
