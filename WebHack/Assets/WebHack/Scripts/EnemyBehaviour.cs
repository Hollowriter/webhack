﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBehaviour : MonoBehaviour {
    public GameObject disparo;
	[SerializeField]
    public Transform target;
	[SerializeField]
	public Transform enemigo;
	private bool hacked;
    private bool direccion;
    [SerializeField]
	private int timerHacked;
	[SerializeField]
	private int seinSpeed;
	private int waiting;
    private int thyShots;
    private bool chocandose;
    [SerializeField]
    private AudioClip disparando;
    [SerializeField]
    private AudioClip hackeoColision;
    [SerializeField]
    private AudioClip hackeado;
    [SerializeField]
    private AudioClip finDelHackeo;

	void Start(){
		hacked = false;
        direccion = false;
        chocandose = false;
		timerHacked = 140;
		waiting = timerHacked;
        thyShots = 0;
	}

	void Update () {
		if (hacked == false) {
			if (enemigo.position.x > target.position.x + 4.0f || enemigo.position.x < target.position.x - 4.0f) {
                Follower();
			} else {
				Shoot ();
			}
            if (enemigo.position.x > target.position.x){
                direccion = false;
				transform.rotation = Quaternion.AngleAxis(180, Vector3.up);
            }
            else if (enemigo.position.x < target.position.x){
                direccion = true;
				transform.rotation = Quaternion.AngleAxis(0, Vector3.up);
            }
		} 
		else if (hacked == true){
            if (waiting == timerHacked) {
                GetComponent<AudioSource>().PlayOneShot(hackeado);
            }
            waiting--;
			if (waiting == 0) {
				waiting = timerHacked;
				hacked = false;
                GetComponent<AudioSource>().PlayOneShot(finDelHackeo);
			}
		}

        RaycastHit hit;

        if (direccion == true)
        {
            if (Physics.Raycast((Vector2)this.transform.position, Vector2.right, out hit, 1.0f, LayerMask.GetMask("cono") | LayerMask.GetMask("pared")))
            {
                chocandose = true;
            }
            else
            {
                chocandose = false;
            }
        }

        if (direccion == false)
        {
            if (Physics.Raycast((Vector2)this.transform.position, Vector2.left, out hit, 1.0f, LayerMask.GetMask("cono")))
            {
                chocandose = true;
            }
            else
            {
                chocandose = false;
            }
        }


    }

	void Follower(){
        Vector3 dir = target.position - this.transform.position;
        if (chocandose == false){
            dir.y = 0.0f;
            dir.Normalize();
            this.transform.position += dir * seinSpeed * Time.deltaTime;
        }
        else {
            this.transform.position = this.transform.position;
        }
	}

	void Shoot(){
        if (/*!(GameObject.Find("BalaEnemiga(Clone)") != null)*/ thyShots <= 0){
            GetComponent<AudioSource>().PlayOneShot(disparando);
            GameObject leDisparo = (GameObject)Instantiate(disparo, transform.position, transform.rotation);
            HackingBehaviour shooting = leDisparo.GetComponent<HackingBehaviour>();
            shooting.DondeVoy(direccion);
        }
        thyShots++;
        if (thyShots > 50){
            thyShots = 0;
        }
    }

	private void OnTriggerEnter(Collider other){
		if (other.tag == "hackeo") {
            GetComponent<AudioSource>().PlayOneShot(hackeoColision);
			hacked = true;
		}  
    }
}
