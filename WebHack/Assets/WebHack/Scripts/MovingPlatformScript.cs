﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// No usar este script para las paredes que te empujan del escenario
public class MovingPlatformScript : MonoBehaviour {
	public float speed = 1;
    public bool sites = false;
	private int direction = 1;
	private bool hacked = false;
    [SerializeField]
    private int limitMoveTimer = 100;
    private int moveTimer = 0;
	private int hackedTimer = 200;
    private const int thyHackedTime = 200;
    private Rigidbody rbd;
    [SerializeField]
    private AudioClip hackeoColision;
    [SerializeField]
    private AudioClip hackeado;
    [SerializeField]
    private AudioClip finDelHackeo;
    private void Awake(){
        rbd = GetComponent<Rigidbody>();
    }
	void FixedUpdate () {
		if (hacked == false) {
            if (sites == false){
                transform.Translate(Vector3.left * speed * DirectionChanger() * Time.deltaTime);
            }else{
                transform.Translate(Vector3.up * speed * DirectionChanger() * Time.deltaTime);
            }
            moveTimer++;
        } else {
            if (hackedTimer == thyHackedTime) {
                GetComponent<AudioSource>().PlayOneShot(hackeado);
            }
			hackedTimer--;
			if (hackedTimer <= 0) {
				hacked = false;
				hackedTimer = thyHackedTime;
                GetComponent<AudioSource>().PlayOneShot(finDelHackeo);
            }
		}
	}
    void OnTriggerEnter(Collider colisionador){
		if (colisionador.tag == "hackeo") {
			hacked = true;
            GetComponent<AudioSource>().PlayOneShot(hackeoColision);
            hackedTimer = thyHackedTime;
        }
        if (this.tag == "MovingWall"){
            if (colisionador.tag == "Player"){
                colisionador.transform.Translate(Vector3.left * speed * (DirectionChanger() * (-1)) * Time.deltaTime);
            }
        }
    }
    private void OnTriggerStay(Collider other){
        if (this.tag == "MovingPlatform"){
            if (other.tag == "Player"){
                //Debug.Log("entra");
                other.transform.SetParent(transform, true);
            }
        }
    }
    void OnTriggerExit(Collider colisionador){
		if (colisionador.tag == "Player") {
			colisionador.transform.SetParent(null, true);
		}
	}
    int DirectionChanger(){
        if (moveTimer >= limitMoveTimer){
            if (direction == 1){
                direction = -1;
            } else {
                direction = 1;
            }
            moveTimer = 0;
        }
        return direction;
    }
}
