﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindBehaviour : MonoBehaviour {
    [SerializeField]
    private int direction;
    private double timeCounter;
    private Vector3 yAngle;

    // Use this for initialization
    void Start () {
        direction = 0;
        timeCounter= 0;
    }
	/*if (pausa == true) {
                Time.timeScale = 1;
                pausa = false;
            }
            else if (pausa == false)
            {
                Time.timeScale = 0;
                pausa = true;
            }
    }*/
	// Update is called once per frame
	void Update () {
        timeCounter += Time.deltaTime;
        if (timeCounter >= 40.0)
        {
            direction = 1;

            transform.rotation = Quaternion.AngleAxis(90.0f, Vector3.up);
            //this.transform.Rotate(yAngle, 180);
            //transform.rotation = Quaternion.AngleAxis(0, Vector3.right);
            timeCounter = 0.0;
        }
        else if (timeCounter >= 20.0)
        {
            direction = 0;
            transform.rotation = Quaternion.AngleAxis(-90.0f, Vector3.up);
            //this.transform.Rotate(yAngle, -180);
            //transform.rotation = Quaternion.AngleAxis(0, Vector3.right);
        }

	}
}
