﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryBehaviour : MonoBehaviour {
    private bool gotcha;
    [SerializeField]
    private string scene;
	void Start () {
        gotcha = false;
	}
	void Update () {
        Victory();
	}
    void Victory() {
        if (gotcha == true){
            if (scene == "Level1" 
                || scene == "Level2" 
                || scene == "JumpingTutorial" 
                || scene == "WalljumpTutorial" 
                || scene == "HackingTutorial"
				|| scene == "CompHackJump"
				|| scene == "CompWallHack"){
                SceneManager.LoadScene(scene);
            }
            else {
                SceneManager.LoadScene("Credits");
            }
        }
    }
    void OnTriggerEnter(Collider other){
        if (other.tag == "Player") {
            gotcha = true;
            Debug.Log("entrando");
        }
    }
}
