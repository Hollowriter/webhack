﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour {
    public GameObject thyPlayer;
    private Vector3 offset;
	void Start () {
        offset = transform.position - thyPlayer.transform.position;
	}
	void LateUpdate () {
        transform.position = thyPlayer.transform.position + offset;
	}
}
