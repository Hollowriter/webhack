﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour {
    private AudioSource thyMusic;
    private void Update(){
        if (!(GetComponent<AudioSource>().isPlaying)) {
            GetComponent<AudioSource>().Play();
        }
    }
}
