﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuSphere : MonoBehaviour {
    private bool start = true;
    [SerializeField]
    private AudioClip mover;
    [SerializeField]
    private AudioClip seleccionar;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Pick();
        //Debug.Log("Start?" + start);
	}

    void Pick()
    {
        if ((Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)) && start == true)
        {
            start = false;
            this.transform.position = this.transform.position += new Vector3(0, -1.5f, 0);
            GetComponent<AudioSource>().PlayOneShot(mover);
        }
        if ((Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) && start == false)
        {
            start = true;
            this.transform.position = this.transform.position += new Vector3(0, 1.5f, 0);
            GetComponent<AudioSource>().PlayOneShot(mover);
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            GetComponent<AudioSource>().PlayOneShot(seleccionar);
            if(start == true)
            {
                SceneManager.LoadScene("Story");
            }

            if(start == false)
            {
                Application.Quit();
            }
        }
    }
}
